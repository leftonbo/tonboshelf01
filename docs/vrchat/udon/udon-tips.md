---
title: Udon個人的知見集
description: Udon 開発によって得た個人的な知見集。
---

Udon 開発によって得た個人的な知見集。

最終更新: **2021/02/24**

## UdonSynced できる変数はワールド全体で約 30 個まで

同期変数は毎秒 5 回固定で送信されるが、送信量の制限のために同期変数にも個数制限がある。

同期変数枠を消費するアセットを導入する場合、アセットの説明文またはソースコードの `[UdonSynced]` 記述回数をよく見通しておこう。スクリプトが1つでも、オブジェクトが複数配置されていれば当然その分同期変数枠を消費する。

`[UdonSynced]` がワールドに30個以上存在する場合、そのワールドはほぼ不安定になる (特にマスターのアバターが非マスターから見て不安定な挙動をする)。

詳細: [変数同期の上限](./sync-variable-limitation.md)

## 変数同期は気合でなんとかする (変数同期関数列挙法)

Synced Variable はパケット消費が激しいのでできるだけ使わない (詳細: [変数同期の上限](./sync-variable-limitation.md)) 。

では変数同期はどうするか…、それは「 **取りうる変数のパターンだけ public 関数を作成する** 」以外の方法は無いと考えたほうが良い

```csharp
public void SyncValue ()
{
    switch (value)
    {
        case 0: this.SendCustomNetworkEvent (All, nameof (SV00)); return;
        case 1: this.SendCustomNetworkEvent (All, nameof (SV01)); return;
        ....
    }
}
```

```csharp
private void SetValue (int value) { /* ~~~ */ }

public void SV00 () => SetValue (0);
public void SV01 () => SetValue (1);
...
```

### 変数同期関数列挙法の利点

- Synced Variable よりも確実性の高い変数同期ができる
  - 主観的観測ではあるが、パケットロス・順番違いの確率が非常に低い
  - ゲームに重要なパラメータ郡はイベント飛ばすほうが確実

### 変数同期関数列挙法の欠点

- 利用できるのが離散的かつ取りうる値が限定されている場合のみ
  - これに該当しない変数は **現状同期不能** と考えたほうが良い
- とりうる値が広いと当然公開イベントも大きくなる
  - コード量が増えるのは当然
  - 公開イベントが多くなることで全体的なパフォーマンスに影響がでるかは不明

## Syncronized Position したオブジェクトの Rigitbody のパラメータは変更不能

Syncronized Position したオブジェクトの Rigitbody は、 **Use Gravity と IsKinematic のパラメータを一切変更できなくなる**。これは Udon 内部の仕様と思われる (Ownerが他者に渡った際に VRChat 側が Rigidbody の設定値を制御している模様)。

## Syncronized Position したオブジェクトの Rigitbody の IsSleep は同期しない

Owner 側で強制的に `Rigidbody.Sleep ()` しても、非 Owner にはそれが**同期されない**。そのため、Owner で `Sleep ()` を使用して空中で物体固定させていても、**非 Owner 視点では物体がその場で落下と位置の復元を繰り返し続けて上下運動する**。

Pickupするまで空中固定させる、といったギミックのためには、Owner/非Owner両方で `Sleep ()` を実行させ続けなければならない。

## UdonBehaviour をつけたオブジェクトは Prefab 化を避ける、またはコード更新のたびに Revert をしておく

理由は不明だが、 U# のコンパイルのたびに UdonBehaviour が勝手に差分を作り始めることがある。気がついたら Prefab 差分が発生していて、 Prefab 側での変更がシーンに適用されていないといったことが多々。

特に**オブジェクトを選択している状態で、そのオブジェクトにある UdonSharp のコードを更新すると、確実に差分が発生する**。コード編集前にオブジェクト選択を外すと安全。

## UdonBehaviour を Prefab 化し、パラメータをオブジェクトごとに変えたい場合、そのパラメータは別のコンポーネントで代用する

UdonBehaviour の変更は**すべてのパラメータの変更とみなされ、 1 つでもパラメータ変更があると Prefab 側の設定をすべて無視する**。

どうしてもシーンに配置した Prefab に各個のパラメータ差分を作りたい場合、 Slider などのパラメータ個別差分が作れるコンポーネントで代用すると良い。

## null チェックは重い

null チェック (というか全てのオブジェクト参照) は実はかなりのパフォーマンスコスト。というのも、オブジェクトへのアクセスのたびに「セキュリティチェック」が行われるからである (対象のオブジェクトがブラックリストに入っていないかチェックしている。要するにチート対策)。

単発イベント上での null チェックはともかく、 `Update ()` での null チェックはするべきではない (50 オブジェクト以上あるとパフォーマンス影響大)。

## 当たり判定が Trigger であれば、同じレイヤー同士の衝突を避ける設定をしておく

例えば `CollideOther` レイヤーを作っておき、 `CollideOther` 同士の衝突を避けるようにすると、他のオブジェクトと重なった際に `OnTriggerStay` が発生しなくなるため、パフォーマンスの改善につながる。

なお、 Trigger ではないものにこの設定を行ってしまうと、**プレイヤーがその上に乗れてしまい、プレイヤーを吹き飛ばせるオブジェクトになってしまう** (プレイヤーの地面判定は、PickUp、UI、UIMenu、MirrorRefrection以外の全てに対して行われる)。

## VRC_PickUp 用のコライダーは、何とも衝突させない専用のレイヤーを作った上で IsTrigger にする

VRChatのレーザー判定が、 `MirrorRefrection` 以外の全てのレイヤーに対して行われることを利用。

例えば `OnlyRaycast` レイヤーを作っておき、 全ての衝突を避けるようにすると、他のPickupと重なった際に `OnTriggerStay` が発生しなくなるため、パフォーマンスの改善につながる。

PickUpに物理挙動をさせたい場合は、子オブジェクトに Collider をもたせ、 `IsTrigger` を `False` に、レイヤーを `PickUp` にすることで対応できる。

## VRC_PickUp の Proximity パラメータはダミー

VRC_PickUp の設定値よりも、 **UdonBehaviour の隠しパラメータにある `Proximity` が優先されるため、 VRC_PickUp の Proximity は全く意味をなしていない**。

Inspector モードを Debug にすることで、この隠されたパラメータを変更できる。

## UnityEngine.UI.Text よりも TMPro.TextMeshPro(UI) を使う

UnityEngine.UI.Text は UI にしか使えず、 Canvas が一々必要だったり、長い日本語を使用した際の改行は非常に怪しい挙動を行う (日本語全文を1つの英単語として数えてしまうため)。

TextMeshPro の方は MeshRenderer 用と UI 用が用意されており、長い日本語を使用しても正しく自動改行してくれる。また、アウトラインや影落としなどの描画オプションが豊富に用意されている。

なお、 TextMeshPro には現在以下の欠点がある。場合によっては UI.Text と使い分けると良いかもしれない。

- 日本語フォントを用意する必要がある。つまり、ワールド容量が上昇する。
- **`enabled` が何故か expose されていない**ため、表示・非表示を直接制御できない。現在は text に string.Empty を入れるか、color を透明にするしかない。
- `transform` も何故か expose されていない。こちらは `GetComponent()` で代用可能。

ちなみに TMP_Text (TextMeshPro、TextMeshProUIの親クラス) は expose されていない。なんでや…

## アバターの頭位置を取得する場合、ジェネリックアバターには気をつける

ジェネリックアバターに頭は存在しないので、頭位置を取得する際に `Vector3.Zero` が帰ってくる (※要検証)。頭に判定を着けたい場合は要注意。

## Interact() 時に Owner を奪って、Interact したプレイヤーを Master が取得する

```
public override void Interact ()
{
    // 現時点のOwnerを取得
    var oldOwner = Networking.GetOwner (this.gameObject);
    // ローカルテスト対策をする
    if (oldOwner == null) return;
    // Owner を奪う
    Networking.SetOwner (Networking.LocalPlayer, this.gameObject);
    // イベントを送信
    this.SendCustomNetworkEvent (All, nameof (OnAnyoneInteract));
}

public void OnAnyoneInteract ()
{
    // これが呼ばれた時点で Owner は既に変更されている
    // 自分がマスターかチェック
    var localPlayer = Networking.LocalPlayer;
    if (localPlayer.IsMaster () == false) return;
    // Owner を取得
    var owner = Networking.GetOwner (this.gameObject);
    // この owner が Interact() したプレイヤーになっている
}
```

安定時であれは SetOwner と Event の順序違いの可能性はほぼゼロである模様。

通信中に LaterJoiner が発生すると、元OwnerがOwnerを奪ってしまう？ (要検証)

## 外部から呼び出さない関数は private にする

`private` を付けると外部から関数を呼び出せないのは勿論、 `SendCustomEvent` や `SendCustomNetworkEvent` で呼び出せない関数になる。

ハック版クライアントのチート対策になるので考慮しておくと良い。

## 期限切れの情報

### VrcPlayerApi はキャッシュしない

(02/24) `Utility.IsValid ()` が実装され、これを用いることでキャッシュした `VrcPlayerApi` が有効かどうか判定できるようになった。

~~様々な場面で `VrcPlayerApi` を取得できるが、これをキャッシュとして保持すると非常に危険。取得した `VrcPlayerApi` に対応するプレイヤーがインスタンスから退出しても、Udon 上では `VrcPlayerApi` は常に正常なものとして保持され続ける。この `VrcPlayerApi` を用いて関数を実行すると、存在しないプレイヤーの関数を実行しようとし、 `NullReferenceException` が発生、Udon が強制終了されてしまう。~~

~~パフォーマンスコストは掛かってしまうが、現状 `VrcPlayerApi` キャッシュせずに逐一取得したほうが安全である。~~

(OnPlayerLeft でキャッシュした  `VrcPlayerApi` のチェックを行うことは一応可能ではある…おそらくは。ただし勿論ローカルのログアウト時にはチェック不能。)

~~近いうちに `VrcPlayerApi.IsValid ()` が実装される見込みであり、この問題は解決されるかもしれない。~~
