---
title: 'Worldよく見るアセット (SDK2)'
description: いろんなところにあるあのギミックはなんだ！？をまとめるページ
---

いろんなところにあるあのギミックはなんだ！？をまとめるページ

原則SDK2用のアセットであり、Udonでは使用不可。

## バンドル (一括セット)

### VRC_Starter Kit

日本製ワールドで頻繁に用いられる「ビデオプレイヤー」「時計」「ナイトモード」「人数カウンター」「ミラー」などがひとまとめに入ったアセット。

迷ったらとりあえず入れてみるべし。

[VRC_Starter Kit](https://booth.pm/ja/items/979837)

## ビデオプレイヤー

### iwaSyncVideo (イワシンク)

Video, Stream 両対応で、URL入力が誰にでもできる動画プレイヤー。

2020/08/23時点での最新版 (v2.4SK) は「[VRC_Starter Kit](https://booth.pm/ja/items/979837)」に同封されている。

- 旧バージョン
    - [v2.3a](https://github.com/ikuko/SyncVideoPlayer_iws)
    - [v2.2s](https://github.com/ziritsu/SyncVideoPlayer_iws)

## その他ギミック

### Qv Pen

ワールド上に書けるペン。既存のペンツールと比べて比較的同期ズレが起きにくい。

アルファ版だが Udon 用のバージョンもある。

[ワールドペン Qv Pen (トレイル、消しゴム機能付き) [VRChat]](https://booth.pm/ja/items/1555789)

#### Qv Pen を Quest 対応させる

1. シェーダーを変更する (VRChat/Mobile/Particle/Alpha Blended が最適、Unlit/ColorだとTrailRendererの色変更が適用されない)
2. Pen の中にある Trail Renderer の Width を 「0.005」にする

[Twitterの投稿](https://twitter.com/vrctaki/status/1239123471159840769)

### vrchat-time-shaders

SDK2環境でも現在時刻が表示できる時計シェーダー。[VRC_Starter Kit](https://booth.pm/ja/items/979837)にも同封されている。

[vrchat-time-shaders](https://github.com/y23586/vrchat-time-shaders)

## 期限切れ

Unity2018アップデート以前で使用されていたが、現在は使用が困難または不可能なもの。

### ExMenu (拡張メニュー)

メニューボタンでテレポートやチャットなどの機能が使え**た**アセット。バーチャルマーケットでも使用されていた。

**現 VRC 環境では正常動作しない**。

[ExMenu（拡張メニュー） v1.1](https://chiugame.booth.pm/items/1510744)
