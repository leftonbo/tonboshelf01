---
title: レビュー＆紹介 トップ
description: LefTonbo が訪れたワールドをピックアップしてレビューしたり解説したりしたものをまとめるページ
---

LefTonbo が訪れたワールドの中で気になったものをピックアップしてレビューしたり解説したりしたものをまとめるページ。

## ゲームワールド

### Murder シリーズ

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  1ゲームの時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **Murder 2**   |  正体隠匿、FPS  |  3-24 (5-10)  |  2-15分  |  プレイ可能 (2020/10/??)  |
|  **Murder 3**   |  正体隠匿、FPS  |  3-24 (5-10)  |  2-15分  |  不安定 (2020/10/??)  |
|  **Murder 4**   |  正体隠匿、FPS  |  3-24 (5-10)  |  2-15分  |  プレイ可能 (2020/02/24)  |
|  **Minecraft Murder Udon**   |  正体隠匿、FPS  |  3-12 (5-10)  |  2-15分  | ？ |

### 人狼系対戦

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  1ゲームの時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **'Among Us' game**   |  正体隠匿、議論  |  4-10 (6-10)  |  5-30分  |  プレイ可能 (2020/12/12)  |
|  **UdonJinRou**  |  手動人狼  |  ？  |  ？  |  ？  |
|  **JINRO 人狼**  |  自動人狼  |  ？  |  ？  |  ？  |
|  **[TonboWerewolf - トンボ人狼](../publishes/tonbowerewolf.md)**  |  自動人狼、自作ワールド  |  ？  |  ？  |  ？  |

### 非対称対戦

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  1ゲームの時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **Freeze Tag**   |  鬼ごっこ  |  2+ (6+)  |  ？  |  ？  |
|  **Zombie Tag**   |  鬼ごっこ、脱落なし  |  2+ (6+)  |  ？  |  ？  |
|  **[Slaughter House](./slaughter-house/index.md)**   |  鬼ごっこ、脱出  |  2-20 (4-8)  |  3-10分  |  プレイ可能 (2020/12/??)  |
|  **[Slaughter House 2](./slaughter-house-2/index.md)**   |  鬼ごっこ、脱出  |  2-10 (4-10)  |  5-12分  |  プレイ可能 (2020/02/24)  |

### 対戦FPS

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  1ゲームの時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **Laser Dome**   |  FPS、脱落なし、VR専用  |  2-10 (4-10)  |  3分～30分  |  ？  |
|  **Shooting Battle VRC**   |  FPS  |  2-20 (4-20)  |  3分～15分  |  プレイ可能 (2020/02/24)  |
|  **Close Quarters Battle**  |  FPS  |  ？  |  ？  |  ？  |
|  **Just Shooting Coasterǃǃǃ - Trial course -**  |  FPS、カジュアル、コースター、JUSTシリーズ  |  1-8  |  3分  |  ？  |

### 対戦バトルアクション

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  1ゲームの時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **[TonboBattlefield 01](../publishes/tonbobattlefield01.md)**  |  バトルアクション、FPS、脱落なし、自作ワールド  |  2-20 (3-20)  |  3-15分  |  プレイ可能 (2020/02/24)  |

### 対戦隠れんぼ

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  1ゲームの時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **Hide & Seek**  |  かくれんぼ、ホラー、洋館  |  ？  |  ？  |  ？  |
|  **Hide and Seek**  |  かくれんぼ、ホラー、廃校  |  ？  |  ？  |  ？  |
|  **Hide-n-seek**  |  かくれんぼ、ホラー、廃工場、広大  |  ？  |  ？  |  ？  |

### その他対戦ゲーム

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  1ゲームの時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **BiRocket Touch and Go**   |  飛行、広大、脱落なし  |  1-4 (2-4)  |  3-8分  |  不安定 (2020/11/??)  |
|  **Thermal Treatment**   |  耐久、物理  |  1+ (4+)  |  3分  |  プレイ可能 (2020/12/12)  |
|  **PEKO PEKO BATTLE**  |  スポーツ、脱落なし  |  ？  |  ？  |  ？  |
|  **Satetu's YouganBall**  |  スポーツ、脱落なし  |  ？  |  ？  |  ？  |
|  **クロックワイズリバーシ**  |  アブストラクト、脱落なし  |  4 (4)  |  15-20分  |  ？  |
|  **Paint Ball (UDON)**  |  カジュアル  |  ？  |  ？  |  ？  |
|  **Balloon Battle**  |  カジュアル  |  ？  |  ？  |  ？  |
|  **おえかきクイズ**  |  クイズ、お絵かき  |  ？  |  ？  |  ？  |
|  **限定しりとり**   |  クイズ、しりとり  |  2+ (3+)  |  5-10分  |  ？  |
|  ~~Udon Falling Fight~~   |  カジュアル、耐久、FPS  |  2+ (2+)  |  1-5分  |  正常プレイ不可 (2020/11/??)  |
|  ~~VRC Battle Royal and Murder~~   |  バトルロイヤル、FPS  |  2+ (4+)  |  5-15分  |  削除済み (2020/04/??)  |

### 協力アドベンチャー

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  推定プレイ時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **DUNGEON OF SORCERESS**  |  アドベンチャー、バトルアクション、ソフトコア  |  1-8 (4)  |  ？  |  プレイ可能 (2020/12/??)  |
|  **CASTLE OF SNOW QUEEN**  |  アドベンチャー、バトルアクション、ソフトコア  |  1-8 (4)  |  ？  |  プレイ可能 (2020/12/??)  |

### 協力FPS

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  推定プレイ時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **Shattered World Episode 1**  |  FPS、ゾンビ、ソフトコア  |  1-8 (4)  |  ？  |  ？  |
|  **Escort!**  |  FPS、ペイロード、死亡なし  |  1-24 (1-24)  |  ？  |  ？  |
|  **VRCDoom**  |  FPS、レトロゲー再現  |  1-8 (1-4)  |  90分  |  ？  |

### その他協力ゲーム

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  推定プレイ時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **Spirit Speak Game**  |  情報伝達  |  2-8 (4-8)  |  3分  |  ？  |
|  **Wavelength**  |  情報伝達  |  ？  |  ？  |  ？  |
|  **パネしり PaneShiri**  |  しりとり、イラスト  |  1+ (3+)  |  ？  |  ？  |

### 謎解き・脱出・1人用ゲーム

| ワールド名  |  ジャンル  |  協力プレイ  |  推定プレイ時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **カラフルダッシュ！**  |  ジャンプアクション、避けゲー  |  なし  |  120分  |  プレイ可能 (2020/12/??)  |
|  **謎解きワールド「555second」**  |  謎解き、複数ステージ  |  可能  |  180分  |  ？  |
|  **VRChat Escape Project 01˸ Training**  |  謎解き、脱出  |  可能  |  120分  |  ？  |
|  **VRChat Escape Project 02˸ Split**  |  謎解き、脱出  |  可能  |  120分  |  ？  |

## ボードゲーム再現

| ワールド名  |  ジャンル  |  必要 (推奨) 人数  |  推定プレイ時間  |  状態  |
| :---: | :---: | :---: | :---: | :---: |
|  **Board Game SUIMIN Room**  |  ボードゲームセット  |  ？  |  ？  |  ？  |
|  **Virtual Lost Legacy［JP］**  |  心理戦、推理  |  ？  |  ？  |  ？  |
|  **インサイダー-INSIDER-**  |  言葉当て、推理、正体隠匿  |  ？  |  ？  |  ？  |
|  **Machi Koro VR**  |  拡大再生産  |  2-4 (2-4)  |  60-90分 |  ？  |
|  **VRC_incan**  |  チキンレース  |  ？  |  ？  |  ？  |
|  **Can't Stopǃǃ**  |  チキンレース  |  ？  |  ？  |  ？  |
|  **王への請願VRC -Um Krone und Kragen-**  |  デッキビルド  |  ？  |  ？  |  ？  |
|  **VRC-Dominion‼**  |  デッキビルド  |  ？  |  ？  |  ？  |
|  **語彙大富豪 in VRChat**  |  大喜利  |  ？  |  ？  |  ？  |

## 風景

| ワールド名  |  ジャンル  |  状態  |
| :---: | :---: | :---: |
|  **Snakes ＆ Ladders**  |  ゲームシーン再現  |  ？  |
|  **Shinkansen Series 0**  |  ？  |  ？  |
|  **何もない場所／Don't forget there․**  |  ？  |  ？  |
|  **Lunar Plants**  |  ？  |  ？  |
|  **Exhibition˸ 1٪ of Virtuality**  |  ？  |  ？  |
|  **UNDERGROUND CITY**  |  ？  |  ？  |
|  **Beyond Reality˸ Alpha**  |  ？  |  ？  |
|  **Beyond Reality˸ Beta**  |  ？  |  ？  |
|  **Elevator VR**  |  ？  |  ？  |
|  **AER Memories of Old**  |  ？  |  ？  |
|  **Twin Spire Village**  |  ？  |  ？  |
|  **Another moon -Party style-**  |  ？  |  ？  |
|  **Mountain Spring**  |  ？  |  ？  |
|  **Halloween Town ǃ**  |  ？  |  ？  |
|  **VirtualFurence Furry Convention Project**  |  ？  |  ？  |
|  **Icebreaktime Room**  |  ？  |  ？  |
|  **Fantasy Inn**  |  ？  |  ？  |
|  **Maintenance room**  |  ？  |  ？  |
|  **ORIGAMI RYOKAN (おりがみ旅館)**  |  ？  |  ？  |
|  **Jungle Island**  |  ？  |  ？  |
|  **Gravity Udon By Kally**  |  ？  |  ？  |
|  **黄昏れ特急 -Big Sunset Express-**  |  ？  |  ？  |
|  **Treehouse in the Shade**  |  ？  |  ？  |
|  **Project Nostalgia**  |  ？  |  ？  |
|  **MLP - The Friendship Express**  |  ？  |  ？  |
|  **Blizzard Igloo**  |  ？  |  ？  |
|  **Tonight's dinner is SUKIYAKIǃ**  |  ？  |  ？  |
|  **Happy Birthday Party**  |  ？  |  ？  |
|  **月光庵 - Gekkouan -**  |  ？  |  ？  |
|  **Happy New Year2020**  |  ？  |  ？  |
|  **Joya no Kane**  |  ？  |  ？  |
|  **終末駅 - post apocalyptic station -**  |  ？  |  ？  |
|  **Japan Street**  |  ？  |  ？  |
|  **ARTIFICIAL TYPHOON**  |  ？  |  ？  |
|  **はじまりの村 - Home village -**  |  ？  |  ？  |
|  **Ene's Cave**  |  ？  |  ？  |
|  **XP Desktop**  |  ？  |  ？  |

## 一発ネタ

| ワールド名  |  ジャンル  |  状態  |
| :---: | :---: | :---: |
|  **NERD MUST DIE**  |  一発ネタ  |  ？  |
|  **Just Do It ǃ**  |  海外ミーム、JUSTシリーズ  |  ？  |
|  **Super excite music world**  |  日本ミーム、音MAD  |  ？  |

## 集会場

| ワールド名  |  ジャンル  |  状態  |
| :---: | :---: | :---: |
|  ~~Fantasy Shukai jou~~  |  集会場  |  削除済み (2020/04/01)  |

## おもちゃ

| ワールド名  |  ジャンル  |  状態  |
| :---: | :---: | :---: |
|  **Satetu‘s FireWorks**  |  ？  |  ？  |
|  **Noriben Fireworks**  |  ？  |  ？  |
|  **King of the Hill**  |  ？  |  ？  |
|  **GR's Shooting Range**  |  ？  |  ？  |
|  **yeet me**  |  ？  |  ？  |
|  **KAROSHI OFFICE**  |  ？  |  ？  |
|  **Restraint room ［拘束部屋］**  |  ？  |  ？  |
|  **VRMC˸ Build ＆ mine blocks**  |  ？  |  ？  |
|  **Kurohige kiki ippatu**  |  ？  |  ？  |
|  **Dice Talk**  |  ？  |  ？  |
|  **I want to know you dice game**  |  ？  |  ？  |

## ホームワールド

|  ワールド名  |  ジャンル  |  状態  |
| :---: | :---: | :---: |
|  **Japanese ryokan -修学旅行-**  |  ？  |  ？  |
|  **A Rainy Night In**  |  ？  |  ？  |
|  **Moody**  |  ？  |  ？  |
|  **Hangout House**  |  ？  |  ？  |
|  **Home Of Dreams**  |  ？  |  ？  |
|  **Tropical Tower**  |  ？  |  ？  |
|  **坪倉家｜Tsubokuras Home**  |  ？  |  ？  |
|  **MEROOM**  |  ？  |  ？  |
|  **Home of the Highlands 山のお家**  |  ？  |  ？  |
|  **The room of the rain**  |  ？  |  ？  |
|  **Home of the Time 時のお家**  |  ？  |  ？  |

## アバター

| ワールド名  |  ジャンル  |  状態  |
| :---: | :---: | :---: |
|  **Himiko Avatar World**  |  総合  |  ？  |
|  **Avali Avatars**  |  アヴァリ (Avali)、宇宙人、ケモノ  |  ？  |
|  **Rikkor and Relax**  |  リコ族、ケモノ  |  ？  |
|  **Beepplanet Alpha**  |  リコ族、ケモノ  |  ？  |
|  **Stayss Birb's Avatars**  |  小鳥、ミーム  |  ？  |

## フィットネス

| ワールド名  |  ジャンル  |  状態  |
| :---: | :---: | :---: |
|  **Smash Contest**  |  ゲーム  |  正常 (2020/12/??)  |
|  **SquatGym**  |  スクワット  |  正常 (2020/12/??)  

## その他

| ワールド名  |  ジャンル  |  状態  |
| :---: | :---: | :---: |
|  **［JP］ Tutorial world**  |  チュートリアル  |  正常 (2020/12/??)  |
|  **Avatar Testingǃǃ**  |  テスト  |  クラシックワールド  |
|  **Just 三面図 ／ Just sanmenzu**  |  テスト、JUSTシリーズ  |  ？  |
