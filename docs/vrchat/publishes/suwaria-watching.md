---
title: Suwaria Watching!
---

![ワールド内で回遊するスワリア](./images/VRChat_1920x1080_2020-07-31_23-57-45.721.png){: loading=lazy }
<small>ワールド内で回遊するスワリア</small>

幻遊童子さんのアバター **[スワリア](https://forestwind.booth.pm/items/1418065)** をテーマにしたワールド。

**[VRChat Home で開く！](https://www.vrchat.com/home/launch?worldId=wrld_af9c7f22-30f6-4468-8150-ac7225cc491e)**

## 制作のきっかけ

ホームワールド用モデル **[アオクジラ](https://booth.pm/ja/items/1522729)** の外側をワールド水泳ギミック **[SwimSystem](https://booth.pm/ja/items/2127684)** で泳げるようにすると面白そうじゃね！？というわけで水泳ワールドを制作。外側に泳がせる生物としてスワリアを設置。

## Q&A

### Swalia なのでは？

名前の元ネタ的にはたぶんそう
