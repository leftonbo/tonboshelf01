---
title: TonboWaitingRoom
---

![ワールドサムネイル](./images/World-TonboLounge01-Image-201842.file_df22029f-45fe-4a55-b0b9-f84941cbdba5.1.png){: loading=lazy }

LefTonbo が初期ホーム用と待機部屋用に制作したワールド。Udon製。

**[VRChat Home で開く！](https://vrchat.com/home/launch?worldId=wrld_69c3e3fe-d935-4c77-a496-bc4376dad4ce)**

## 設置物

### 現実時間と同期する Skybox

Low Poly Farland Skies と、Udonによるローカル時刻を用いた、リアルタイムの Skybox 更新ギミック。昼は明るく、夜は暗く。

### UdonSyncVideo

Udon製のビデオプレイヤー。
