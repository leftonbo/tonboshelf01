---
title: ホーム
---

# TonboShelf01

このページは LefTonbo の知見などをまとめています。

## サイト管理者

![プロフィール画像](/assets/images/avatar-leftonbo-256.png)

- Author: LefTonbo
- :fontawesome-brands-twitter: Twitter: [@LefTonbo](https://twitter.com/LefTonbo/)

## 本ドキュメントのライセンス

本ドキュメントの内容は、特に明記無い限り
[CC BY 4.0 :fontawesome-brands-creative-commons::fontawesome-brands-creative-commons-by:](https://creativecommons.org/licenses/by/4.0)
のライセンスに従います。但し、以下の内容については適用外とします。

- イラストやスクリーンショットに映り込んだアバターやキャラクター
- その他、ページ内で明記された例外
