---
title: 創作意欲はどこから生まれるのか
---

# 創作意欲はどこから生まれるのか

- ①無自覚なうちに
- ②何かに触発されて
- ③感情の昂ぶりを表現する手段として

## 参考リンク

[創作意欲はどこから生まれ、どのように維持され、どう死ぬのか　1](https://note.mu/zawa_music_03/n/n9d337ae6b881)
