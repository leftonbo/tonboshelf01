---
title: UniRx 情報まとめ
---

# UniRx 情報まとめ

**UniRx** - Unity (あるいはC#) で使用可能な、宣言的なプログラミング記述を可能にするライブラリ。

[UniRx入門](https://qiita.com/toRisouP/items/2f1643e344c741dd94f8)

[オペレーター逆引き](https://qiita.com/toRisouP/items/3cf1c9be3c37e7609a2f)
