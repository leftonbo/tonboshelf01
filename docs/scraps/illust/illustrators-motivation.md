---
title: 絵描きがやる気を出すには
---

# 絵描きがやる気を出すには

- 紙に書く
- 仮眠をとる
- 健康に気をつける
- 他の人の絵を見る
- ネットにうｐる
- 描くための環境作り
- 常に絵について考える

## 参考リンク

[（絵描きのための）やる気を出す７つの方法](http://moetatsu.com/motivation)
