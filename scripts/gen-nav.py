# -*- coding:utf-8 -*-
"""
gen-nav.py
replace 'nav' in mkdocs.yml with contents in document folder
"""
import sys
import getopt
import os
import glob
import frontmatter
import ruamel.yaml
from ruamel.yaml.scalarstring import SingleQuotedScalarString, DoubleQuotedScalarString
from collections import OrderedDict
import codecs
import shutil

def ruamel_represent_odict(dumper, instance):
    return dumper.represent_mapping('tag:yaml.org,2002:map', dict(instance))

ruamel.yaml.representer.RoundTripRepresenter.add_representer(
    OrderedDict,
    ruamel_represent_odict)

# full

def gen_nav_page_od(path, dirname):
    name = os.path.basename(path)
    post = frontmatter.load(path, encoding="utf-8")
    if 'title' in post.keys():
        title_sq = SingleQuotedScalarString(post['title'])
        value_sq = SingleQuotedScalarString(dirname + name)
        od = OrderedDict()
        od[title_sq] = value_sq
        return od
    else:
        return SingleQuotedScalarString(dirname + name)

def gen_nav_dir_od(path, dirname):
    ret = []
    files = glob.glob(os.path.join(path, '*'))
    for f in files:
        if os.path.isdir(f):
            name = os.path.basename(f)
            if not name.startswith('_'):
                internalList = gen_nav_dir_od(f, dirname + name + '/')
                if len(internalList) > 0:
                    name_sq = SingleQuotedScalarString(name)
                    od = OrderedDict()
                    od[name_sq] = internalList
                    ret.append(od)
        elif os.path.splitext(f)[1] == '.md':
            ret.append(gen_nav_page_od(f, dirname))
    return ret

# simple

def find_files_page(path, dirname):
    name = os.path.basename(path)
    post = frontmatter.load(path, encoding="utf-8")
    if 'title' in post.keys():
        title_sq = SingleQuotedScalarString(post['title'])
        value_sq = SingleQuotedScalarString(dirname + name)
        od = OrderedDict()
        od[value_sq] = title_sq
        return od
    else:
        return SingleQuotedScalarString(dirname + name)

def find_files_dir(path: str, dirname: str, ret: list):
    files = glob.glob(os.path.join(path, '*'))
    for f in files:
        if os.path.isdir(f):
            name = os.path.basename(f)
            if not name.startswith('_'):
                find_files_dir(f, dirname + name + '/', ret)
        elif os.path.splitext(f)[1] == '.md':
            ret.append(find_files_page(f, dirname))

# main

def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        opts,args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.error.msg:
        print(msg)
        print("for help use --help")
        return 2

    for o,a in opts:
        if o in ("-h","--help"):
            print(__doc__)
            return 0

    filename = 'mkdocs.yml'
    if not os.path.exists(filename):
        print('error: not found file (mkdocs.yml)')
        return 0

    backup_filename = 'mkdocs.yml.bak'
    if os.path.exists(backup_filename):
        os.remove(backup_filename)

    filename = 'mkdocs.yml'
    shutil.copy2(filename, backup_filename)

    f = open('mkdocs.yml', 'r', encoding="utf-8")
    data = ruamel.yaml.round_trip_load(f, preserve_quotes=True)
    f.close()

    nav = None;
    if len(args) == 0:
        nav = gen_nav_dir_od('./docs/', '')
    else:
        nav = []
        for a in args:
            path = './docs/' + a
            if os.path.isdir(path):
                name_sq = SingleQuotedScalarString(a)
                od = OrderedDict()
                od[name_sq] = gen_nav_dir_od(path, a + '/')
                nav.append(od)
            else:
                nav.append(gen_nav_page_od(path, ''))

    f = codecs.open(filename, 'w+', 'utf-8')
    data['nav'] = nav
    f.write(ruamel.yaml.round_trip_dump(data, default_flow_style=False, encoding='utf-8', allow_unicode=True, explicit_start=False).decode())
    f.close()

    return 0

if __name__ == '__main__':
    sys.exit(main())
